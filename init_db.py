#! /usr/bin/python3

import os
from datetime import datetime, timedelta
from random import randrange, random
from asyncio import run
from argparse import ArgumentParser
from sqlalchemy.ext.asyncio import create_async_engine
from hw1_webapp.settings import DB_FILE, ENGINE_URL
import hw1_webapp.db as db

webapp_engine = create_async_engine(ENGINE_URL, echo=True)

stores_num = 15

streets = [
    'Лесная',
    'Яблоневая',
    'Еловая',
    'Тополиная',
    'Дубовая',
    'Сосновая'
]

items_num = 15

item_names = [
    'Карандаши',
    'Краски',
    'Фломастеры',
    'Кисти',
    'Мелки',
    'Ластики',
    'Ручки'
]

sales_num = 50

def drop_db():
    if os.path.exists(DB_FILE):
        os.remove(DB_FILE)

async def init_db():
    drop_db()
    conn = await webapp_engine.connect()
    await db.init_tables(conn)

async def sample_db():
    await init_db()
    conn = await webapp_engine.connect()
    # Заполняем таблицу store случайными даннами на основе массива улиц
    for i in range(stores_num):
        await db.store_insert(conn, 'ул. ' + streets[randrange(len(streets))] +
             ', д. ' + str(randrange(1, 50)))
    # Заполняем таблицу item случайными даннами на основе массива товаров
    # т.к. есть вероятность, что item_name будет не уникален, то проверяем,
    # что функция item_insert успешно отработала и вернула результат
    i = 0
    while (i < items_num):
        item_insert_result = await db.item_insert(conn,
            item_names[randrange(len(item_names))] + ' №' + str(randrange(1, 50)),
            random()*1000
        )
        if (item_insert_result):
            i += 1
    # Заполняем таблицу sales случайными даннами с датами в разбросом на 45 дней от текущей
    time_start = int((datetime.now() - timedelta(days=45)).timestamp())
    time_end = int(datetime.now().timestamp())
    for i in range(sales_num):
        await db.sales_insert(conn,
            randrange(1, items_num + 1),
            randrange(1, stores_num + 1),
            datetime.fromtimestamp((randrange(time_start, time_end )))
        )
    await conn.close()

if __name__ == '__main__':
    parser = ArgumentParser(description='DB related shortcuts')
    parser.add_argument("-i", "--init",
                        help="Create empty database with tables",
                        action='store_true')
    parser.add_argument("-d", "--drop",
                        help="Drop database",
                        action='store_true')
    parser.add_argument("-s", "--sample",
                        help="Create DB with sample data",
                        action='store_true')
    args = parser.parse_args()

    if args.init:
        run(init_db())
    elif args.drop:
        drop_db()
    elif args.sample:
        run(sample_db())
    else:
        run(sample_db())
    #    parser.print_help()