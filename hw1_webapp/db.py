from sqlalchemy import (
    MetaData, Table, Column, ForeignKey,
    Integer, Float, String, DateTime
)
from sqlalchemy import event
from sqlalchemy.engine import Engine
from sqlalchemy.sql import func

# для upsert, но не работает, см. ниже
# from sqlalchemy.dialects.sqlite import insert as sqlite_upsert

'''Table store { // магазины торговой сети
  id integer [pk, increment]
  address varchar
}

Table item { // товарные позиции (уникальные наименования)
  id integer [pk, increment]
  name varchar [unique]
  price float
}

Table sales { // продажи
  id integer [pk, increment]
  sale_time datetime [not null, default: `now()`]
  item_id integer [ref: < item.id]
  store_id integer [ref: < store.id]
}'''

meta = MetaData()

store = Table(
    'store', meta,
    Column('id', Integer, primary_key=True, autoincrement=True),
    Column('address', String(200), nullable=False)
)

item = Table(
    'item', meta,
    Column('id', Integer, primary_key=True, autoincrement=True),
    Column('name', String(200), nullable=False, unique=True),
    Column('price', Float, nullable=False)
)

sales = Table(
    'sales', meta,
    Column('id', Integer, primary_key=True, autoincrement=True),
    Column('sale_time', DateTime,  nullable=False, server_default=func.now()),
    # ForeignKey
    Column('item_id', Integer, ForeignKey('item.id'), nullable=False),
    # ForeignKey
    Column('store_id', Integer, ForeignKey('store.id'), nullable=False)
)

# Для поддержки внешних ключей в sqlite
# https://docs.sqlalchemy.org/en/20/dialects/sqlite.html#foreign-key-support
#@event.listens_for(Pool, "connect")
#async def set_sqlite_pragma(dbapi_connection, connection_record):
#    cursor = dbapi_connection.cursor()
#    cursor.execute("PRAGMA foreign_keys=ON")
#    print("PRAGMA foreign_keys=ON")
#    cursor.close()

@event.listens_for(Engine, "connect")
async def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    print("PRAGMA foreign_keys=ON")
    cursor.close()

async def init_tables(conn):
    await conn.run_sync(meta.create_all)
    await conn.close()

async def store_insert(conn, address):
    try:
        result = await conn.execute(store.insert(),
            { 'address': address }
        )
    except:
        await conn.rollback()
    else:
        await conn.commit()
        return result

async def item_insert(conn, name, price):
    # С конфликтами все плохо:
    # https://docs.sqlalchemy.org/en/14/dialects/sqlite.html#sqlite-on-conflict-insert
    # при этом не работает даже просто добавление "ON CONFLICT"
    # в текстовом запросе для версии 1.4.45, по-этому пока буду решать по-другому
    '''stmt = sqlite_upsert(item).values(
        {'name': name,
        'price': price}
    )
    stmt = stmt.on_conflict_do_update(
        index_elements=['name'], set_=dict(price=stmt.excluded.price)
    )
    await conn.execute(stmt)'''
    try:
        result = await conn.execute(item.insert(),
            { 'name': name, 'price': price }
        )
    except:
        await conn.rollback()
    else:
        await conn.commit()
        return result

async def sales_insert(conn, item_id, store_id, sale_time):
    try:
        result = await conn.execute(sales.insert(),
            { 'sale_time': sale_time, 'item_id': item_id, 'store_id': store_id }
        )
    except:
        await conn.rollback()
    else:
        await conn.commit()
        return result
