import os

BASE_DIR = str(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

DB_FILE = BASE_DIR + '/db.sqlite'
ENGINE_URL = 'sqlite+aiosqlite:///' + DB_FILE