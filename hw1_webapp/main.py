import uvicorn
from fastapi import FastAPI
import databases
from pydantic import BaseModel, constr
import db
from settings import ENGINE_URL
from sqlalchemy import func, select, desc

database = databases.Database(ENGINE_URL)

app = FastAPI()

class Store(BaseModel):
    id: int
    address: constr(min_length=2, max_length=200)

class Item(BaseModel):
    id: int
    name: constr(min_length=2, max_length=200)
    price: float

class Sale(BaseModel):
    id: int
#    sale_time: datetime
    item_id: int
    store_id: int

class SaleIn(BaseModel):
    item_id: int
    store_id: int

class StoreTop(BaseModel):
    id: int
    address: constr(min_length=2, max_length=200)
    income: float

class ItemTop(BaseModel):
    id: int
    name: constr(min_length=2, max_length=200)
    sales_amount: int


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()

@app.get("/")
async def root_get():
    return {"message": "Hello from HW1"}

@app.get("/stores/", response_model=list[Store])
async def stores_get():
    query = db.store.select()
    return await database.fetch_all(query)

@app.get("/items/", response_model=list[Item])
async def items_get():
    query = db.item.select()
    return await database.fetch_all(query)

# GET: топ 10 самых доходных магазинов за месяц (id + адрес + суммарная выручка)
# SELECT store.id, store.address, SUM(item.price) as income FROM sales, item, store
#  WHERE sales.sale_time > datetime('now','-1 month') AND item.id == sales.item_id
#  AND store.id == sales.store_id GROUP BY store.id ORDER BY income DESC LIMIT 10;
@app.get("/stores/top/", response_model=list[StoreTop])
async def get_stores_top():
    query = select(
            db.store.c.id, db.store.c.address, func.sum(db.item.c.price).label('income')
        ).where(
            db.sales.c.sale_time > func.datetime('now', '-1 month')
        ).where(
            db.item.c.id == db.sales.c.item_id
        ).where(
            db.store.c.id == db.sales.c.store_id
        ).group_by(db.store.c.id).order_by(desc('income')).limit(10)
    print(query)
    return await database.fetch_all(query)

# GET: топ 10 самых продаваемых товаров (id + наименование + количество проданных товаров)
# SELECT item.id, item.name, COUNT(*) as sales_amount FROM sales, item
#  WHERE item.id == sales.item_id GROUP BY item.id ORDER BY sales_amount DESC LIMIT 10;
@app.get("/items/top/") #, response_model=list[ItemTop])
async def get_items_top():
    query = select(
            db.item.c.id, db.item.c.name, func.count().label('sales_amount')
        ).where(
            db.item.c.id == db.sales.c.item_id
        ).group_by(db.item.c.id).order_by(desc('sales_amount')).limit(10)
    return await database.fetch_all(query)

@app.post("/sales/", response_model=Sale)
async def sales_post(sale: SaleIn):
    query = db.sales.insert().values(item_id=sale.item_id, store_id=sale.store_id)
    last_record_id = await database.execute(query)
    return {**sale.dict(), "id": last_record_id}

if __name__ == '__main__':
    uvicorn.run(app, host="127.0.0.1", port=8000)
